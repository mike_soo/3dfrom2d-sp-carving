
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Vector.hpp"
#include "Base.hpp"
#include "Draw.hpp"
#include "Controller.hpp"
#include "Vue.hpp"
#include "VoxelG.hpp"
#include "Utils.hpp"
#include "Ligne.hpp"
#include "Camera.hpp"

#define WIDTH  600

#define HEIGHT 600

// Définition de la couleur de la fenêtre
#define RED   0.8
#define GREEN 0.8
#define BLUE  0.8
#define ALPHA 1




Draw d;

// Mouse
//Cordonnés 2D (interfaz)
Vector mouseRecup;
Vector *ptClicked;

//Cordonnés 3D (opengl)
Vector mouseCord3D;

bool mousegAc = false , mousemAc = false; 
bool clickSurPoint = false;
bool zoom_modif;

/* ==================== ROTATION ========================*/

Vector p1(0, 0, 0);
Vector p2(0, 0, 0);
Vector p1p2(0, 0, 0);
float **Ru;
bool deuxiemeCli;
float zoom=2;
Vector vo0(0 , 0 , 0);
Vector vx0(1 , 0 , 0);
Vector vy0(0 , 1 , 0);
Vector vz0(0 , 0 , 1);
Base base0(vo0 , vx0 , vy0 , vz0);

Vector vso(0 , 0 , 0);
Vector vsx(1 , 0 , 0);
Vector vsy(0 , 1 , 0);
Vector vsz(0 , 0 , 1);
Base base1(vso , vsx , vsy , vsz);


float ortho = 15;
float theta = M_PI / 128;

/*==================TP_IMAGE_UPMC ====================*/

vector < VoxelG* > voxels;
vector < Ligne* > lignes;
vector < Camera* > cameras;

/*====================================================*/

// Touche echap (Esc) permet de sortir du programme
#define KEY_ESC 27

// Entêtes de fonctions
void init_scene();
void render_scene();
GLvoid initGL();
GLvoid window_display();
GLvoid window_reshape(GLsizei width, GLsizei height);
GLvoid window_key(unsigned char key, int x, int y);


int main(int argc, char **argv)
{
  cout<<"Main Init"<<endl;
  Ru = new float *[3];

  for (int i = 0; i < 3; i++)
  {
    Ru[i] = new float [3];
  }
  Utils::readOFFVoxels(&base1 , voxels , lignes , cameras , string("voxels.off"));
  cout<<"numbers of voxels read :"<<voxels.size()<<endl;

  cout << "Ru initialised" << endl;

  // initialisation  des paramètres de GLUT en fonction
  // des arguments sur la ligne de commande

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH | GLUT_MULTISAMPLE);

  // définition et création de la fenêtre graphique, ainsi que son titre
  glutInitWindowSize(WIDTH, HEIGHT);
  glutInitWindowPosition(1500, 0);
  glutCreateWindow(" Voxelisation TP IMAGE - UPMC");

  // initialisation de OpenGL et de la scène
  initGL();
  init_scene();
  glewInit();
     
  // choix des procédures de callback pour
  // le tracé graphique
  glutDisplayFunc(&window_display);
  // le redimensionnement de la fenêtre
  glutReshapeFunc(&window_reshape);
  // la gestion des événements clavier
  glutKeyboardFunc(&keyboard);

  // gestion des événements boutons de souris
  glutMouseFunc(mouseB);
  // gestion des mouvements souris
  glutMotionFunc(mouseM);

  // la boucle prinicipale de gestion des événements utilisateur
  cout << "Start glutMainLoop" << endl;
  glutMainLoop();

  return 1;
}

// initialisation du fond de la fenêtre graphique : noir opaque
GLvoid initGL()
{
  glClearColor(RED, GREEN, BLUE, ALPHA);
}

// Initialisation de la scene. Peut servir à stocker des variables de votre programme
// à initialiser
void init_scene()
{
  glClearColor(0, 0, 0, 0);

  glDepthMask(GL_TRUE);
  glEnable(GL_DEPTH_TEST);
  glDepthRange(0.0f,1.0f);
  glDepthFunc(GL_LEQUAL); 

  glEnable(GL_COLOR_MATERIAL);
  /*
  float mcolor[] = { 0.2f, 0.0f, 1.0f, 1.0f };
  float specReflection[] = { 0.1f, 0.1f, 0.8f, 1.0f };

  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, mcolor);
  glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specReflection);

  glMateriali(GL_FRONT, GL_SHININESS, 96);*/
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
  glShadeModel(GL_SMOOTH); //GL_FLAT GL_SMOOTH
  glEnable(GL_MULTISAMPLE);



}

// fonction de call-back pour l´affichage dans la fenêtre

GLvoid window_display()
{

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  

 
   // if(preDraw)
   //  {
   //    glDrawBuffer(GL_FRONT);
   //    preDraw=false;
   //  }
   
   // else 
  glDrawBuffer(GL_BACK);
   
    
   
 

  
  glLoadIdentity();
  glScalef(zoom, zoom, zoom);

  if (mousegAc || mousemAc)
  {
    getOGLPosSai( mouseRecup.getX() , mouseRecup.getY() );
  }

  gestionRotation();

  // C'est l'endroit où l'on peut dessiner. On peut aussi faire appel
  // à une fonction (render_scene() ici) qui contient les informations
  // que l'on veut dessiner
  render_scene();


  // trace la scène grapnique qui vient juste d'être définie
  // glFlush();

  glutSwapBuffers();
}

// fonction de call-back pour le redimensionnement de la fenêtre

GLvoid window_reshape(GLsizei width, GLsizei height)
{
  glViewport(0, 0, width, height);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  // ici, vous verrez pendant le cours sur les projections qu'en modifiant les valeurs, il est
  // possible de changer la taille de l'objet dans la fenêtre. Augmentez ces valeurs si l'objet est
  // de trop grosse taille par rapport à la fenêtre.
  glOrtho(-ortho, ortho, -ortho, ortho, -ortho, ortho);

  // toutes les transformations suivantes s´appliquent au modèle de vue
  glMatrixMode(GL_MODELVIEW);
}

// fonction de call-back pour la gestion des événements clavier

GLvoid window_key(unsigned char key, int x, int y)
{
  switch (key) {
  case KEY_ESC:
    exit(1);
    break;

  default:
    printf ("La touche %d n´est pas active.\n", key);
    break;
  }
}



void render_scene()
{

  for(unsigned int i = 0 ; i < voxels.size() ; i ++)
  {
    // cout<<*voxels.at(i)<<endl;
    voxels.at(i)->draw_surfaces(&d);
    voxels.at(i)->draw_lines(&d , 1 , 1 , 1);
  }

  for(unsigned int i = 0 ; i < lignes.size() ; i++)
  {
    lignes.at(i)->drawLigne(&d , 0 , 1 , 0);
  }
  
  for (unsigned int i =0 ; i < cameras.size() ; i++)
  {
    cameras.at(i)->draw(&d);
  }

  // // base 1
  d.drawLine(&vso , &vsx , 1.0 , 0.0 , 0.0);
  d.drawLine(&vso , &vsy , 0.0 , 1.0 , 0.0);
  d.drawLine(&vso , &vsz , 0.0 , 0.0 , 1.0);

}
