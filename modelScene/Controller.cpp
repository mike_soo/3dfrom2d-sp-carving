#include "Controller.hpp"


extern bool mousegAc , mousemAc ,deuxiemeCli;
extern Vector mouseRecup;
extern bool clickSurPoint;
extern bool zoom_modif;
extern float zoom;
extern float angle;
extern Vector p1_tr_2;

void mouseB(int button, int state, int x, int y)
{

  mouseRecup.setX(x);
  mouseRecup.setY(y);
  switch (button)
  {

  case (GLUT_MIDDLE_BUTTON):
    if (state == GLUT_UP)
    {
      // cout<<"mousmDeac"<<endl;
      mousemAc = false;
      deuxiemeCli = false;
    }

    else if (state == GLUT_DOWN)
    {
      mousemAc = true;
      glutPostRedisplay();
    }
    break;

  case (GLUT_LEFT_BUTTON):

    if (state == GLUT_DOWN)
    {
      mousegAc = true;
      
      glutPostRedisplay();
    }
    else if (state == GLUT_UP)
    {
      mousegAc = false;
      clickSurPoint = false;
      glutPostRedisplay();
    }
    break;


  }
}

void mouseM(int x, int y)
{ 
  // if (mousegAc)
  // { //indicatif
    // std::cout<<" Souris a:"<<x<<" "<<y<<endl;
    mouseRecup.setX(x);
    mouseRecup.setY(y);
    glutPostRedisplay();
  // }
}


void keyboard(unsigned char key, int x, int y) 
{

 

  if(key == 27)
  {
    exit(1);
  }
  
  else if(key =='z')
  {
    if(zoom_modif)
    {
      cout<<"zoom_modif : off"<<endl;
      zoom_modif = false;
    }
    else
    {
      cout<<"zoom_modif : on"<<endl;
      zoom_modif =true;
    }
  }

  
  
  else if(key == 43 ) // +
  {

    if(zoom_modif)
      zoom += 0.25;
           
  }
  
  else if(key == 45 ) // -
  {
      
    if (zoom_modif)
      zoom -= 0.25;
        
  }      
  else
    printf ("La touche %d n´est pas active.\n", key);
    
  cout<<"La touche "<<(int)key << "a été detectée"<<endl;
  glutPostRedisplay();
    
     
  
}