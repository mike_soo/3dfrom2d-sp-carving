#include "Ligne.hpp"

Ligne::Ligne( Vector* p1 , Vector* p2)
{
    this->p1 = p1;
    this->p2 = p2;
}

void Ligne::drawLigne(Draw *d ,float r , float g , float b)
{
    d->drawLine(this->p1 , this->p2 , r , g , b);

}