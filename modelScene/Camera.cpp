#include "Camera.hpp"

Camera::Camera( Ligne *ligne1 , Ligne *ligne2 , Ligne *ligne3)
{
    this->ligne1 = ligne1;
    this->ligne2 = ligne2;
    this->ligne3 = ligne3;
}

void Camera::draw(Draw *d)
{
    this->ligne1->drawLigne(d , 1 , 0 , 0);
    this->ligne2->drawLigne(d , 0 , 1 , 0);
    this->ligne3->drawLigne(d , 0 , 0 , 1);
}