#include "Draw.hpp"

Draw::Draw()
{
	this->points = NULL;
	this->nb_ps = 0;
}
Draw::Draw(Vector *points , size_t nb_ps) 
{
	this->points = points;
	this->nb_ps = nb_ps;
}

void Draw::setPoints(Vector *points, size_t nb_ps)
{
	this->points = points;
	this->nb_ps=nb_ps;
}

void Draw::drawPoint(Vector * p)
{
	glPointSize(5);	
	glBegin(GL_POINTS);
		glVertex3f(p->getX(),p->getY(),p->getZ());
	glEnd();
}

void Draw::drawCurve()
{

	glBegin(GL_LINE_STRIP);
	for (size_t i =0 ; i < nb_ps ; i++)
	{
		cout<<"\t"<<i<<endl;
  		glVertex3f(								
  					this->points[i].getX(),    
  					this->points[i].getY(),    
  					this->points[i].getZ());
	}

	glEnd();
}

void Draw::drawCurve(Vector * points[] , size_t nb_ps , Base &baseSelect)
{

	glBegin(GL_LINE_STRIP);
	Vector pointPourAffich;
	
	for (size_t i =0 ; i < nb_ps ; i++)
	{
		pointPourAffich = *points[i];
		pointPourAffich.setBase(baseSelect);
		
	
  		glVertex3f(								
  					pointPourAffich.getX(),    
  					pointPourAffich.getY(),    
  					pointPourAffich.getZ()
  					);
		


	}

	glEnd();

}

void Draw::drawCurve(Vector points[] , size_t nb_ps , float r, float g , float b)
{
	glColor3f(r , g , b);
	glBegin(GL_LINE_STRIP);
	Vector pointPourAffich;
	
	for (size_t i =0 ; i < nb_ps ; i++)
	{
  		glVertex3f(								
  					points[i].getX(),    
  					points[i].getY(),    
  					points[i].getZ()
  					);
		


	}

	glEnd();

}

void Draw::drawCurve(Vector * points[] , size_t nb_ps , Base &baseSelect , float r , float g , float b)
{

	glColor3f( r , g , b );
	this->drawCurve(points , nb_ps , baseSelect);
}


void Draw::drawCurve(Vector points[] , size_t nb_ps , Base &base)
{


	Vector * ppoints[nb_ps];
	for(unsigned int i = 0 ; i < nb_ps ; i++)
	{
		ppoints[i]=&points[i];
	}
	this->drawCurve(ppoints , nb_ps , base);
}

void Draw::drawCurve(Vector points[] , size_t nb_ps , Base &baseSelect , float r , float g , float b)
{	

	Vector * ppoints[nb_ps];
	for(unsigned int i = 0 ; i < nb_ps ; i++)
	{
		ppoints[i]=&points[i];
	}
	this->drawCurve(ppoints , nb_ps , baseSelect , r , g , b);	
}

void Draw::drawPoint(Vector * p, float r, float g, float b)
{	
	// cout<<"drawing point ("<<p->getX()<<","<<p->getY()<<","<<p->getZ()<<")"<<endl;
	
	glPointSize(10);
	glColor3f(r,g,b);
	glBegin(GL_POINTS);
		glVertex3f(p->getX(),p->getY(),p->getZ());
	glEnd();
}		

void Draw::drawPoint(Vector * p , Base *base , float r , float g , float b)
{	
	// cout<<"drawing point ("<<p->getX()<<","<<p->getY()<<","<<p->getZ()<<")"<<endl;
	Base *baseSauvegarde = p->getBase();
	p->setBase(*base);

	glPointSize(5);
	glColor3f(r,g,b);
	glBegin(GL_POINTS);
		glVertex3f(p->getX(),p->getY(),p->getZ());
	glEnd();

	p->setBase(*baseSauvegarde);
	}		

void Draw::drawLine(Vector *o , Vector *m)
{	
	glLineWidth(2.5);
	
	glBegin(GL_LINES);
	glVertex3f(o->getX(), o->getY(), o->getZ());
	glVertex3f(m->getX(), m->getY(), m->getZ());
	glEnd();
}

void Draw::drawLine(Vector *o , Vector *m , float r , float g , float b)
{
	glColor3f(r, g, b);
	this->drawLine(o,m);
}

void Draw::drawLine(Vector *o , Vector *m , Base *base , float r , float g , float b)
{
	Vector co = *o;
	Vector cm = *m;
	co.setBase(*base);
	cm.setBase(*base);
	glColor3f(r , g , b);
	this->drawLine(&co , &cm );
}

void Draw::drawBase(Base *base , float r , float g , float b)
{	

	this->drawLine(&base->getO() , &base->getVi() , r , g , b);
	this->drawLine(&base->getO() , &base->getVj() , r , g , b);
	this->drawLine(&base->getO() , &base->getVk() , r , g , b);
}

void Draw::drawSurface(Vector *p1 , Vector *p2 , Vector *p3 , Vector *p4)
{
	glBegin(GL_QUADS);
		glVertex3f(p1->getX() , p1->getY() , p1->getZ());
		glVertex3f(p2->getX() , p2->getY() , p2->getZ());
		glVertex3f(p3->getX() , p3->getY() , p3->getZ());
		glVertex3f(p4->getX() , p4->getY() , p4->getZ());
	glEnd();
}

void Draw::drawSurface(Vector *p1 , Vector *p2 , Vector *p3 , Vector *p4 , float r , float g , float b)
{
	glColor3f(r , g , b);
	drawSurface(p1,p2,p3,p4);

}

void Draw::drawTriangleFan(unsigned int nb_vertex_bord, Vector *center , Vector bord[] , short int direction)
{
	glColor3f(0.5,0.5,0.5);
	
	glBegin(GL_TRIANGLE_FAN);
	glVertex3f(center->getX() , center->getY() , center->getZ());
	

	if(direction == 1)
	{
		for(unsigned int i = 0 ; i <= nb_vertex_bord ; i++)
		{
			glVertex3f(bord[i%nb_vertex_bord].getX() , bord[i%nb_vertex_bord].getY() , bord[i%nb_vertex_bord].getZ());
		}
	}
	else if(direction == -1)
	{
		for( int i = nb_vertex_bord - 1 ; i>= 0 ; i--)
		{
			
			glVertex3f(bord[i].getX() , bord[i].getY() , bord[i].getZ());
		}
		glVertex3f(bord[nb_vertex_bord-  1].getX() , bord[nb_vertex_bord-  1].getY() , bord[nb_vertex_bord-  1].getZ() );
	}
	glEnd();
}

void Draw::drawTrianglesVbo(GLfloat* data_vertex, GLuint* data_index , GLsizeiptr sizeofdata_vertex , GLsizeiptr sizeofdata_index)
{
	// Génération des buffers
	glColor3f(1.0 , 0.0 , 0.0);
    unsigned int buffers[2];
    
    // Génération des buffers
    glGenBuffers( 2, buffers );

    // Buffer d'informations de vertex
    glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeofdata_vertex, data_vertex, GL_DYNAMIC_DRAW);

    // Buffer d'indices
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeofdata_index, data_index, GL_DYNAMIC_DRAW);
  
    // // Utilisation des données des buffers
    glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
    glVertexPointer( 3, GL_FLOAT, 0, 0 );
    glColorPointer( 3, GL_FLOAT, 0, 0 );


    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[1]);

    // // Activation d'utilisation des tableaux
    glEnableClientState( GL_VERTEX_ARRAY );
    glEnableClientState( GL_COLOR_ARRAY );

    // // Rendu de notre géométrie
    glDrawElements(GL_TRIANGLES, sizeofdata_index, GL_UNSIGNED_INT, 0);

    glDisableClientState( GL_COLOR_ARRAY );
    glDisableClientState( GL_VERTEX_ARRAY );



}


