#ifndef LIGNE_HPP
#define LIGNE_HPP
#include "Vector.hpp"
#include "Draw.hpp"
#include "Base.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <unistd.h>
extern Base base0;
extern Base base1;

class Ligne 
{
private:
	Vector * p1;
    Vector * p2;
public:
	Ligne( Vector* p1 , Vector * p2);
	void drawLigne(Draw *d ,float r , float g , float b);
};

// ostream& operator<<(ostream& os, const Ligne& Ligne);  
#endif