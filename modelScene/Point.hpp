#ifndef POINT_HPP
#define POINT_HPP

class Point 
{

private :
  	double _x,_y,_z;
  
public:
	Point();
	Point(const double x, const double y , const double z);
	virtual double getX();
	virtual double getY();
	virtual double getZ();
};
#endif //IO_WAVE_HPP
