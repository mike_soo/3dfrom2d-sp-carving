#ifndef VOXELG_HPP
#define VOXELG_HPP
#include "Vector.hpp"
#include "Draw.hpp"
#include "Base.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <unistd.h>
extern Base base0;
extern Base base1;

class VoxelG 
{
private:
	Base *base; 
	Vector *origine;
	double longueur;
	vector<VoxelG*> * filsVoxelG;
	vector<Vector*> * pointsVoxelG;
	vector<Vector*> * pointsContenus;
	vector<Vector*> * pointsMaillages;
public:
	VoxelG(Base *base , Vector* origine , double longeur);
	VoxelG(Base *base , Vector* origine , double longeur , vector <Vector*>* pointsMaillages);
	const double getXorig()const ;
	const double getYorig()const ;
	const double getZorig()const ;
	const double getLongueur() const;
	void init_valeurs(Base *base ,Vector *origine     , double longueur);
	Vector* getOrigine();
	void addPoint(Vector *p);
	void draw_surfaces(Draw *d);
	void draw_lines(Draw *d ,float r , float g , float b);
	void draw_points(Draw *d , float r , float g , float b);
	void octree( int res , int res_max , Draw *d );
	bool contientPoint(Vector * p);
	void afficheVoxels(Draw *d ,unsigned prof_cour , unsigned prof_max , float r , float g , float );

	vector<Vector* >* getPointsContenus();

};

ostream& operator<<(ostream& os, const VoxelG& voxelG);  
#endif