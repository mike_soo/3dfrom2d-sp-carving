#include "VoxelG.hpp"


VoxelG::VoxelG(Base *base , Vector* origine , double longueur)
{
	init_valeurs(base, origine , longueur);
}

const double VoxelG::getXorig() const 
{
	return this->origine->getX();
}

const double VoxelG::getYorig() const
{
	return this->origine->getY();
}

const double VoxelG::getZorig() const
{
	return this->origine->getZ();
}

const double VoxelG::getLongueur() const
{
	return this->longueur;
}

VoxelG::VoxelG(Base *base , Vector* origine , double longueur , vector <Vector*>* pointsMaillages)
{
	
	init_valeurs(base, origine , longueur);
	this->pointsMaillages= pointsMaillages;
	this->pointsContenus = new vector<Vector*> ();
	this->filsVoxelG = new vector<VoxelG*>();
	
}


Vector * VoxelG::getOrigine()
{
	return this->origine;
}


void VoxelG::draw_surfaces(Draw *d)
{
	d->drawSurface(pointsVoxelG->at(0) , pointsVoxelG->at(1) , pointsVoxelG->at(2) , pointsVoxelG->at(3), 1.0 , 0.0 , 0.0); //face

	d->drawSurface(pointsVoxelG->at(4) , pointsVoxelG->at(7) , pointsVoxelG->at(6) , pointsVoxelG->at(5), 1.0 , 0.0 , 0.0);	//arriere

	d->drawSurface(pointsVoxelG->at(2) , pointsVoxelG->at(6) , pointsVoxelG->at(7) , pointsVoxelG->at(3), 1.0 , 0.0 , 0.0); //droite

	d->drawSurface(pointsVoxelG->at(5) , pointsVoxelG->at(1) , pointsVoxelG->at(0) , pointsVoxelG->at(4), 1.0 , 0.0 , 0.0); //gauche

	d->drawSurface(pointsVoxelG->at(5) , pointsVoxelG->at(6) , pointsVoxelG->at(2) , pointsVoxelG->at(1), 1.0 , 0.0 , 0.0); //dessus

	d->drawSurface(pointsVoxelG->at(4) , pointsVoxelG->at(0) , pointsVoxelG->at(3) , pointsVoxelG->at(7), 1.0 , 0.0 , 0.0);

}

void VoxelG::draw_points(Draw *d , float r , float g , float b)
{
	for(unsigned int i = 0 ; i < this->pointsVoxelG->size(); i ++)
	{
		d->drawPoint(this->pointsVoxelG->at(i), r , g , b);
		// cout<<*(this->pointsVoxelG->at(i))<<endl;
	}
}

void VoxelG::draw_lines(Draw *d ,float r , float g , float b)
{
	d->drawLine(pointsVoxelG->at(1) , pointsVoxelG->at(2) , r , g , b);
	d->drawLine(pointsVoxelG->at(2) , pointsVoxelG->at(3) , r , g , b);
	d->drawLine(pointsVoxelG->at(3) , pointsVoxelG->at(0) , r , g , b);
	d->drawLine(pointsVoxelG->at(0) , pointsVoxelG->at(1) , r , g , b);
	//arriere
	d->drawLine(pointsVoxelG->at(5) , pointsVoxelG->at(6) , r , g , b);
	d->drawLine(pointsVoxelG->at(6) , pointsVoxelG->at(7) , r , g , b);
	d->drawLine(pointsVoxelG->at(7) , pointsVoxelG->at(4) , r , g , b);
	d->drawLine(pointsVoxelG->at(4) , pointsVoxelG->at(5) , r , g , b);

	//left
	d->drawLine(pointsVoxelG->at(1) , pointsVoxelG->at(5) , r , g , b);
	d->drawLine(pointsVoxelG->at(5) , pointsVoxelG->at(4) , r , g , b);
	d->drawLine(pointsVoxelG->at(4) , pointsVoxelG->at(0) , r , g , b);
	d->drawLine(pointsVoxelG->at(0) , pointsVoxelG->at(1) , r , g , b);
	
	//right		
	d->drawLine(pointsVoxelG->at(2) , pointsVoxelG->at(6) , r , g , b);
	d->drawLine(pointsVoxelG->at(6) , pointsVoxelG->at(7) , r , g , b);
	d->drawLine(pointsVoxelG->at(7) , pointsVoxelG->at(3) , r , g , b);
	d->drawLine(pointsVoxelG->at(3) , pointsVoxelG->at(2) , r , g , b);

	//dessus
	d->drawLine(pointsVoxelG->at(5),pointsVoxelG->at(6) , r , g , b);
	d->drawLine(pointsVoxelG->at(6),pointsVoxelG->at(2) , r , g , b);
	d->drawLine(pointsVoxelG->at(2),pointsVoxelG->at(1) , r , g , b);
	d->drawLine(pointsVoxelG->at(1),pointsVoxelG->at(5) , r , g , b);

	//dessou
	d->drawLine(pointsVoxelG->at(0),pointsVoxelG->at(4) , r , g , b);
	d->drawLine(pointsVoxelG->at(4),pointsVoxelG->at(7) , r , g , b);
	d->drawLine(pointsVoxelG->at(7),pointsVoxelG->at(3) , r , g , b);
	d->drawLine(pointsVoxelG->at(3),pointsVoxelG->at(0) , r , g , b);

}


void VoxelG::init_valeurs(Base * base , Vector *origine , double longueur)
{
	this->base = base;
	this->origine = origine;
	this->longueur=longueur;
	
	
	Vector * pointVoxelG;
	this->pointsVoxelG = new vector <Vector*> ();
	this->pointsVoxelG->push_back(origine);

	pointVoxelG = new Vector( 
						origine->getXr()            , 
						origine->getYr() + longueur , 
						origine->getZr()  	        , *base);
	this->pointsVoxelG->push_back(pointVoxelG);

	pointVoxelG = new Vector( 
						origine->getXr() + longueur , 
						origine->getYr() + longueur , 
						origine->getZr() 	        , *base);
	this->pointsVoxelG->push_back(pointVoxelG);

	pointVoxelG = new Vector( 
						origine->getXr() + longueur , 
						origine->getYr()  		    , 
						origine->getZr()            , *base);
	this->pointsVoxelG->push_back(pointVoxelG);

	pointVoxelG = new Vector( 
						origine->getXr()  		    , 
						origine->getYr() 		    , 
						origine->getZr() + longueur , *base);
	this->pointsVoxelG->push_back(pointVoxelG);

	pointVoxelG = new Vector( 
						origine->getXr() 		    , 
						origine->getYr() + longueur , 
						origine->getZr() + longueur , *base);
	this->pointsVoxelG->push_back(pointVoxelG);

	pointVoxelG = new Vector( 
						origine->getXr() + longueur , 
						origine->getYr() + longueur , 
						origine->getZr() + longueur , *base);
	this->pointsVoxelG->push_back(pointVoxelG);

	pointVoxelG = new Vector( 
						origine->getXr() + longueur , 
						origine->getYr() 		    , 
						origine->getZr() + longueur , *base);
	this->pointsVoxelG->push_back(pointVoxelG);
}



void VoxelG::addPoint(Vector *p)
{
	this->pointsContenus->push_back(p);
}


void VoxelG::octree( int res , int res_max , Draw *d )
{

	
	for (vector<Vector*>::iterator it=pointsMaillages->begin(); 
                              it!=pointsMaillages->end(); ++it )
	{
		// cout<<"VoxelG::octree() : verifie point "<< **it<<endl;
		if(this->contientPoint(*it)) 
		{
			
			this->pointsContenus->push_back(*it);
			(*it)->estDansVoxelG(this);
		  	// cout<<"VoxelG::octree() : contientPoint "<<**it<<endl;	
		  	// it = pointsMaillages->erase(it);
		}
		// else 
		//   ++it;
	}
	
	this->draw_lines(d , 0.0 , 1.0 , 0.0);

	/*  next voxel ===============================> */
	if(res < res_max && this->pointsContenus->size()> 0 )
	{
		
		Vector *origine_centree =NULL;
		
		VoxelG *voxelSuivant=new VoxelG(base , origine , longueur / 2.0 , pointsContenus);
		filsVoxelG->push_back ( voxelSuivant );
		
		origine_centree =new Vector(*origine);	
		origine_centree->setY(origine_centree->getYr() + (longueur / 2.0));
		voxelSuivant=new VoxelG(base , origine_centree , longueur / 2.0 , pointsContenus);
		filsVoxelG->push_back ( voxelSuivant );

		
		origine_centree =new Vector(*origine);
		origine_centree->setY(origine_centree->getYr() + (longueur / 2.0));
		origine_centree->setX(origine_centree->getXr() + (longueur / 2.0));
		voxelSuivant=new VoxelG(base , origine_centree , longueur / 2.0 , pointsContenus);
		filsVoxelG->push_back ( voxelSuivant );
		
		
		origine_centree =new Vector(*origine);
		origine_centree->setX(origine_centree->getXr() + (longueur / 2.0));
		voxelSuivant=new VoxelG(base , origine_centree , longueur / 2.0 , pointsContenus);
		filsVoxelG->push_back ( voxelSuivant );
		
		
		origine_centree =new Vector(*origine);
		origine_centree->setZ(origine_centree->getZr() + (longueur / 2.0));
		voxelSuivant=new VoxelG(base , origine_centree , longueur / 2.0 , pointsContenus);
		filsVoxelG->push_back ( voxelSuivant );
		
		
		origine_centree =new Vector(*origine);
		origine_centree->setY(origine_centree->getYr() + (longueur / 2.0));
		origine_centree->setZ(origine_centree->getZr() + (longueur / 2.0));
		voxelSuivant=new VoxelG(base , origine_centree , longueur / 2.0 , pointsContenus);
		filsVoxelG->push_back ( voxelSuivant );
		
		
		origine_centree =new Vector(*origine);
		origine_centree->setZ(origine_centree->getZr() + (longueur / 2.0));
		origine_centree->setY(origine_centree->getYr() + (longueur / 2.0));
		origine_centree->setX(origine_centree->getXr() + (longueur / 2.0));
		voxelSuivant=new VoxelG(base , origine_centree , longueur / 2.0 , pointsContenus);
		filsVoxelG->push_back ( voxelSuivant );
		
		
		origine_centree =new Vector(*origine);
		origine_centree->setZ(origine_centree->getZr() + (longueur / 2.0));
		origine_centree->setX(origine_centree->getXr() + (longueur / 2.0));
		voxelSuivant=new VoxelG(base , origine_centree , longueur / 2.0 , pointsContenus);
		filsVoxelG->push_back ( voxelSuivant );
		
		
		
		
		
		for(unsigned int i = 0 ; i  < filsVoxelG->size() ; i ++)
		{
			// cout<<"FILS VOXEL "<<i<<" :" <<endl;
			filsVoxelG->at(i)->octree(res + 1 , res_max , d);
		}

	}




}


void VoxelG::afficheVoxels(Draw *d ,unsigned prof_cour , unsigned prof_max, float r , float g , float b)
{
	// string tabs = "";
	// for(unsigned int i=0 ; i < prof_cour ; i++)
	// 	tabs+= "\t";

	// cout<<tabs<<"VoxelG::afficheVoxels() :"<<endl;
	// cout<<tabs<<"INFO NOEUD COURANT"<<endl; 
	// for(unsigned int i = 0 ; i < pointsVoxelG->size() ; i++)
	// {
		// cout<<tabs<<*pointsVoxelG->at(i)<<endl;
	// }
	// cout<<tabs<<"VoxelG::afficheVoxels() : AFFICHE FILS VOXEL"<<endl;
	
	float modif_coul =  1.0 - ( (float) prof_cour/ ((float)prof_max));

	if(	 pointsContenus->size()>0 && filsVoxelG->size() == 0)
		this->draw_lines(d , modif_coul , modif_coul , b);
	

	for(unsigned int i = 0 ; i < filsVoxelG->size() ; i++ )
	{
		// cout<<tabs<<"FILS : "<<i<<endl; 
		filsVoxelG->at(i)->afficheVoxels(d , prof_cour + 1 , prof_max , r , g , b);
	}	

	// cout<<tabs<<"VoxelG::afficheVoxels() : END"<<endl;
}



bool VoxelG::contientPoint(Vector * p)
{
	return 
	   this->pointsVoxelG->at(0)->getX() <= p->getX() 
	&& this->pointsVoxelG->at(0)->getY() <= p->getY() 
	&& this->pointsVoxelG->at(0)->getZ() <= p->getZ() 
	
	&& this->pointsVoxelG->at(1)->getX() <= p->getX() 
	&& this->pointsVoxelG->at(1)->getY() >= p->getY() 
	&& this->pointsVoxelG->at(1)->getZ() <= p->getZ() 
	
	&& this->pointsVoxelG->at(2)->getX() >= p->getX() 
	&& this->pointsVoxelG->at(2)->getY() >= p->getY() 
	&& this->pointsVoxelG->at(2)->getZ() <= p->getZ() 
	
	&& this->pointsVoxelG->at(3)->getX() >= p->getX() 
	&& this->pointsVoxelG->at(3)->getY() <= p->getY() 
	&& this->pointsVoxelG->at(3)->getZ() <= p->getZ() 

	&& this->pointsVoxelG->at(4)->getX() <= p->getX() 
	&& this->pointsVoxelG->at(4)->getY() <= p->getY() 
	&& this->pointsVoxelG->at(4)->getZ() >= p->getZ() 

	&& this->pointsVoxelG->at(5)->getX() <= p->getX() 
	&& this->pointsVoxelG->at(5)->getY() >= p->getY() 
	&& this->pointsVoxelG->at(5)->getZ() >= p->getZ() 

	&& this->pointsVoxelG->at(6)->getX() >= p->getX() 
	&& this->pointsVoxelG->at(6)->getY() >= p->getY() 
	&& this->pointsVoxelG->at(6)->getZ() >= p->getZ() 

	&& this->pointsVoxelG->at(7)->getX() >= p->getX() 
	&& this->pointsVoxelG->at(7)->getY() <= p->getY() 
	&& this->pointsVoxelG->at(7)->getZ() >= p->getZ() ;
	
}


vector<Vector* >* VoxelG::getPointsContenus()
{
	return this->pointsContenus;
}

ostream& operator<<(ostream& os, const VoxelG& voxelG)  
{  
    os <<voxelG.getXorig()<<"  "<< voxelG.getYorig() << "  " << voxelG.getZorig() << "  " << " "<<voxelG.getLongueur()<< endl;  
    return os;  
}  