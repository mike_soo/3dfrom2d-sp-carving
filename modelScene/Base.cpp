#include "Base.hpp"
#include "Vector.hpp"
Base::Base()
{
	this->o  = new Vector(0,0,0);
	this->vi = new Vector(1,0,0);
	this->vj = new Vector(0,1,0);
	this->vk = new Vector(0,0,1);

}
Base::Base(Vector& o,Vector& vi, Vector& vj , Vector& vk)
{
	this->o  = &o;
	this->vi = &vi;
	this->vj = &vj;
	this->vk = &vk;
}

Vector& Base::getO()const
{
	return *o;
}

Vector& Base::getVi() const
{	
	// cout<<"\t\t\tthis->getVi= "<<endl;
	return *vi;
}
Vector& Base::getVj() const
{
	// cout<<"\t\t\tthis->getVj "<<endl;
	return *vj;
}
Vector& Base::getVk() const
{
	// cout<<"\t\t\tthis->getVk "<<endl;
	return *vk;
}