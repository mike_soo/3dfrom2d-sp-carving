#!/usr/bin/python3
import numpy as np
import cv2
from World3d import * 
from Camera import *

        
if __name__ == '__main__':
    world3d = World3d()
    world3d.createEnsVoxels()
    world3d.laserTirTest()
    # 
    # print("main subdivisionTest start")
    world3d.subdivisionTest()


    Rcam1 =np.array([
        [  -0.294080240622522	, 0.955780734282920	   ,  7.00016595456265e-06],
        [    0.377296805771389	, 0.116095630625678	   , -0.918786659080514   ],
        [   -0.878159400354005	,-0.270194360642906    , -0.394754449052401   ]])  
    
    Rcam2 =np.array(
        [  [-0.0318,   -0.7070,    0.7065],
           [ 0.9995,   -0.0220,    0.0230],
           [-0.0007,    0.7069,    0.7073]])

    Rcam3 =np.array(
        [  [ 0.7068,   -0.5493,    0.4457],
           [ 0.7074,    0.5491,   -0.4451],
           [-0.0003,    0.6299,    0.7767]])

    Rcam4 =np.array(
        [  [ 0.8674,    0.3684,   -0.3346],
           [-0.4977,    0.6415,   -0.5838],
           [-0.0004,    0.6729,    0.7397]])

    Rcam5 =np.array(
        [  [ 0.3214,    0.6700,   -0.6692],
           [-0.9470,    0.2272,   -0.2273],
           [-0.0003,    0.7068,    0.7074]])

    Rcam6 =np.array(
        [  [-0.3504,    0.7199,   -0.5992],
           [-0.9366,   -0.2695,    0.2238],
           [-0.0004,    0.6396,    0.7687]])

    Rcam7 =np.array(
        [  [-0.9989,   -0.0360,    0.0292],
           [ 0.0464,   -0.7745,    0.6308],
           [-0.0001,    0.6315,    0.7754]])

    T1=np.array([ -91.8327357637484 ,    6.63224503802932 , 624.614917198851 ])
    T2=np.array([ -86.4823917930422 ,	-143.978863005533 ,	294.052063251369 ])
    T3=np.array([ -113.816709116167 ,	-185.504789955298 ,	287.292467102703 ])
    T4=np.array([ -19.6439185499219 ,	-294.750352606067 ,	411.521262733187 ])
    T5=np.array([ 24.6524504682981  ,	-276.492809615645 ,	426.488660843830 ])
    T6=np.array([ 108.722835309754  ,	-215.317240655115 ,	317.377734014180 ])
    T7=np.array([ 70.4794094786755  ,	-107.597030762113 ,	224.767481653703 ])

    

    K = np.array( [[1641.68794060267 ,0	                ,0],
                  [0	             ,2402.80542290159  ,0],
                  [961.960667510611	 ,1961.42047908535  ,1]]).T

    Klass = np.array([
        [2129.96923750224	, 0	                , 959.549094036105],
        [0	                , 2041.67534399379	, 436.998896965219],
        [0	                , 0	                , 1               ],

    ]

    )

    camera1 = Camera(Klass , Rcam1 , T1 , cv2.imread("../imgs/Cathedrale_1.png" , cv2.IMREAD_COLOR ) , world3d)
    camera2 = Camera(K , Rcam2 , T2 , cv2.imread("../imgs/Cathedrale_2.png" , cv2.IMREAD_COLOR ) , world3d)
    camera3 = Camera(K , Rcam3 , T3 , cv2.imread("../imgs/Cathedrale_3.png" , cv2.IMREAD_COLOR ) , world3d)
    camera4 = Camera(K , Rcam4 , T4 , cv2.imread("../imgs/Cathedrale_4.png" , cv2.IMREAD_COLOR ) , world3d)
    camera5 = Camera(K , Rcam5 , T5 , cv2.imread("../imgs/Cathedrale_5.png" , cv2.IMREAD_COLOR ) , world3d)
    camera6 = Camera(K , Rcam6 , T6 , cv2.imread("../imgs/Cathedrale_6.png" , cv2.IMREAD_COLOR ) , world3d)
    camera7 = Camera(K , Rcam7 , T7 , cv2.imread("../imgs/Cathedrale_7.png" , cv2.IMREAD_COLOR ) , world3d)
    
    world3d.addCamera(camera1)
    world3d.addCamera(camera2)
    world3d.addCamera(camera3)
    world3d.addCamera(camera4)
    world3d.addCamera(camera5)
    world3d.addCamera(camera6)
    world3d.addCamera(camera7)

    # camera1.tirImage()
    # camera1.pixelToWorld(1133 , 824.1)
    # camera1.pixelToWorld(995, 801)
    # camera1.pixelToWorld(861.5, 779.6)
    
    camera1.worldToPixel(0 , 0 , 0 )
    camera1.worldToPixel(1 , 0 , 0 ) # (601 , 500)
    # camera1.tirImage()

    world3d.toFile()