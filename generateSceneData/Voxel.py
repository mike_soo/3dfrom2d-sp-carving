

class Voxel:
    def __init__(self , x=0 , y=0 , z=0 , l=0):
        self.x = x
        self.y = y
        self.z = z
        self.l = l
        self.v = 0
        self.eff = False
    def __str__(self):
        return '(' + str(round(self.x,4)) +' , '+ str(round(self.y,4)) +' , '+ str(round(self.z,4)) + ')'
    def setInfo(self , x , y , z , l):
        self.x = x
        self.y = y
        self.z = z
        self.l = l
        self.v = 0

    def subdivise(self):
        arrayOfVoxels=[]
        
        arrayOfVoxels.append(Voxel( self.x             , self.y             , self.z             , self.l/2.    ))
        arrayOfVoxels.append(Voxel( self.x + self.l/2. , self.y             , self.z             , self.l/2.    ))
        arrayOfVoxels.append(Voxel( self.x             , self.y + self.l/2. , self.z             , self.l/2.    ))
        arrayOfVoxels.append(Voxel( self.x + self.l/2. , self.y + self.l/2. , self.z             , self.l/2.    ))

        arrayOfVoxels.append(Voxel( self.x             , self.y             , self.z + self.l/2. , self.l/2.    ))
        arrayOfVoxels.append(Voxel( self.x + self.l/2. , self.y             , self.z + self.l/2. , self.l/2.    ))

        arrayOfVoxels.append(Voxel( self.x             , self.y + self.l/2. , self.z + self.l/2. , self.l/2.    ))
        arrayOfVoxels.append(Voxel( self.x + self.l/2. , self.y + self.l/2. , self.z + self.l/2. , self.l/2.    ))

        # print("Voxel.subdivise(): subdivised=")

        # for i in range(0 , 8):
        #     print(str(arrayOfVoxels[i]))

        return arrayOfVoxels


