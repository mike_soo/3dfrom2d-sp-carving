from Vecteur import *
import numpy as np
from numpy.linalg import inv
from Laser import *
class Camera:
    def __init__(self , K , R , T , image , world3d):
        self.image = image
        vx = R[0 , :] 
        vy = R[1 , :] 
        vz = R[2 , :] 
        v0 = T

        self.vx = Vecteur(Point(v0[0] , v0[1] , v0[2]) , 
                          Point(vx[0] , vx[1] , vx[2]))
        self.vy = Vecteur(Point(v0[0] , v0[1] , v0[2]) , 
                          Point(vy[0] , vy[1] , vy[2]))
        self.vz = Vecteur(Point(v0[0] , v0[1] , v0[2]) , 
                          Point(vz[0] , vz[1] , vz[2]))
        
        # self.repereMatLab2World3d(self.vx)
        # self.repereMatLab2World3d(self.vy)
        # self.repereMatLab2World3d(self.vz)

        self.K = K

        # self.R = R.transpose()
        self.R = R
        self.T = T.reshape(3,1)


        self.RT  = np.concatenate((self.R , self.T) , axis = 1)
        extraLine = np.array([[0 , 0 , 0 , 1]])
        extraLine1 = [[0] , [0] , [0]]
        self.RT = np.concatenate((self.RT , extraLine) , 0)
        # print("self.RT=")
        # print(self.RT)
        self.K   = np.concatenate((self.K  , extraLine1), 1)
        # print("self.K")
        # print(self.K)
        self.pinvRTinvK = np.linalg.pinv(self.RT) @  np.linalg.pinv(self.K)

        self.world3d = world3d

    def worldToPixel(self , x , y , z):
        
        x3d = np.array([[x] , [y] , [z] , [1]])

        # cam1Pos = np.array([[-76] , [-279] , [-100]])
        # print("cam1Pos.norm=")
        # print(np.linalg.norm(cam1Pos))

        x3dc = (self.RT @ x3d)
        print("Camera.worldToPixel()")
        x3dc = x3dc / x3dc[3,0]
        
        # print ("x3dc=")
        # print(x3dc)
        # print ("x3dcNorme=")
        # print(np.linalg.norm(x3dc))

        x2d = (1/x3dc[2,0]) * (self.K @ self.RT @ x3d)
        
        print("worldToPixel():")
        print(x2d / x2d[2])

    def pixelToWorld(self , i , j):
        x2d = [[i] , [j] , [1]]
        x3d = self.pinvRTinvK @ x2d
    
        x3d = x3d / x3d[3, 0]
        print("x3d=")
        print(x3d)

        return Point(x3d[0,0] , x3d[1,0] , x3d[2,0] )

    def repereMatLab2World3d(self , vecteur):
        # Passage de repère matlab vers repère opengl 
        vecteur.pointOrig.x = -vecteur.pointOrig.x
        ytemp = vecteur.pointOrig.y
        vecteur.pointOrig.y = -vecteur.pointOrig.z
        vecteur.pointOrig.z = -ytemp
        
        vecteur.pointVec.x = -vecteur.pointVec.x
        ytemp = vecteur.pointVec.y
        vecteur.pointVec.y = -vecteur.pointVec.z
        vecteur.pointVec.z = -ytemp
    
    def tirImageEl(self , inds , pointOrigTir , laser):
        pointArrivTir = self.pixelToWorld(inds[0] , inds[1])
        vecTir = Vecteur(pointOrigTir , pointArrivTir) 
        tir = laser.tir(vecTir , self.world3d.ensVoxels , self.world3d.ensVoxels.cote - (self.world3d.ensVoxels.cote * 1./10) )
        self.world3d.addTir(tir)
        

    def tirImage(self):
        pointOrigTir = Point(self.vx.pointOrig.x , self.vx.pointOrig.y , self.vx.pointOrig.z)
        laser  = Laser()
        
        
        # Contient l'image sans le fond 
        indexesFaceImage = np.where(self.image != [  255 , 212 , 147 ])
        nbCols = np.shape(indexesFaceImage)[1]
        print("nbCols")
        print(nbCols)
        print("indexesFaceImage[0]")
        print(indexesFaceImage[0])
        print("indexesFaceImage[1]")
        print(indexesFaceImage[1])

        indexesFaceImage = np.array([indexesFaceImage[0] , indexesFaceImage[1]])
        
        np.apply_along_axis(self.tirImageEl, 0, indexesFaceImage , pointOrigTir , laser)

        # for i in range(self.image.shape[0]):
        #     for j in range(self.image.shape[1]):
        #         k = self.image[i,j]
        #         if(( k != [147 , 212 , 255]).all()):
        #             pointArrivTir = self.pixelToWorld(i , j)
                    
        #             vecTir = Vecteur(pointOrigTir , pointArrivTir) 
        #             tir = laser.tir(vecTir , self.world3d.ensVoxels , self.ensVoxels.cote - (self.ensVoxels.cote * 1./10) )
        #             self.world3d.addTir(tir)

        # for i in range(len(A[0])):
        #     print(self.image[A[0][i] , A[1][i]])
            
        

                # print(k)        
        # cond = (self.image == [147 212 255])
        # for i in self.image:
        #     for j in i:
        #         pass
        #         # print(j);

        
        

    def toFile(self , fVoxels):
        
        fVoxels.write(
            str(self.vx.pointOrig.x)  + " " + str(self.vx.pointOrig.y) + " " + str(self.vx.pointOrig.z) + " "
            + str(self.vx.pointVec.x) + " " + str(self.vx.pointVec.y) + " " + str(self.vx.pointVec.z) + " "
            + str(self.vy.pointVec.x) + " " + str(self.vy.pointVec.y) + " " + str(self.vy.pointVec.z) + " "
            + str(self.vz.pointVec.x) + " " + str(self.vz.pointVec.y) + " " + str(self.vz.pointVec.z) + "\n")
