from Point import *
from Tir import *

class Laser:
    typeEfface = 1
    typeAugm   = 2
    def tir(self , vecTir , ensVoxel3d , pas , type = typeAugm , cptLimit=200) :
        voxelNonIntercept = True
        pointCourant = Point(vecTir.pointOrig.x , vecTir.pointOrig.y , vecTir.pointOrig.z)
        cpt = 1
        pointDepart = Point(vecTir.pointOrig.x , vecTir.pointOrig.y , vecTir.pointOrig.z)
        while (voxelNonIntercept):
            # print('Laser.tir(): pointCourant=' + str(pointCourant))
            if (ensVoxel3d.intersect(pointCourant , type )):
                voxelNonIntercept = False
            cpt = cpt + 1
            if(cpt > cptLimit):
                break
            pointCourant.x = (vecTir.pointVec.x * pas ) + pointCourant.x 
            pointCourant.y = (vecTir.pointVec.y * pas ) + pointCourant.y
            pointCourant.z = (vecTir.pointVec.z * pas ) + pointCourant.z 
        
        pointArrive = Point(pointCourant.x , pointCourant.y , pointCourant.z)
        return Tir(pointDepart , pointArrive)
