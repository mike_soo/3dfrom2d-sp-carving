    
class EnsTirs:
    def __init__(self):
        self.tirs = []
    
    def addTir(self , tir):
        self.tirs.append(tir)

    def toFile(self):
        fVoxels = open("./voxels.off", "a")
        
        fVoxels.write(str(len(self.tirs)) + "\n")
        for tir in self.tirs: 
            fVoxels.write(
            str(tir.pointDepart.x) + " " + str(tir.pointDepart.y) + " " + str(tir.pointDepart.z) + " " 
            + str(tir.pointArrive.x) + " " + str(tir.pointArrive.y) + " " + str(tir.pointArrive.z) + "\n" )

